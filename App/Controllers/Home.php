<?php

namespace App\Controllers;

use \Core\View;
use \App\Models\User;
use \App\Models\Country;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Home extends \Core\Controller {


	public function home () {
		View::renderTemplate ( 'Home/index.html' );
	}

	/**
	 * Show the index page
	 *
	 * @return void
	 */
	public function index () {
		//echo 'userlist';
		$user        = new User();
		$userList    = $user->userList ();
		$countries   = new Country();
		$countryList = $countries->countryList ();

		View::renderTemplate ( 'Home/index.html',
			[
				'users'     => $userList,
				'countries' => $countryList
			] );
	}

	public function newUser () {
		$countries   = new Country();
		$countryList = $countries->countryList ();
		View::renderTemplate ( 'Signup/newuser.html',
			[
				'countries' => $countryList
			] );
	}

	public function create () {
		$user = new User();
		if ( $user->addUser () ) {
			View::renderTemplate ( 'Signup/success.html' );
		}
	}


	public function update () {
		$user = new User();
		if ( $user->updateUser () ) {
			View::renderTemplate ( 'Signup/success.html' );
		}
	}

	public function remove () {
		$id = explode ( '/', $_SERVER['REQUEST_URI'] );
		//echo $id[2];
		$user = new User();
		if ( $user->delete ( $id[2] ) ) {
			View::renderTemplate ( 'Home/index.html' );
		};
	}



	public function edit(){
		$id = explode ( '/', $_SERVER['REQUEST_URI'] );
		//echo $id[2];
		$user = new User();
		$userId = $user->findUser ($id[2]);
		$countries   = new Country();
		$countryId = $countries->countryId ($id[2]);
			View::renderTemplate ( 'Signup/edit.html',[
				'user'=>$userId,
				'country'=>$countryId
			]);

	}

}
