<?php

/**
 * Front controller
 *
 * PHP version 7.0
 */

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';


/**
 * Error and Exception handling
 */
//error_reporting(E_ALL);
//set_error_handler('Core\Error::errorHandler');
//set_exception_handler('Core\Error::exceptionHandler');


/**
 * Sessions
 */
session_start();


/**
 * Routing
 */
$router = new Core\Router();

// Add the routes
$router->add('/',['controller'=>'Home', 'action'=>'home'] );
$router->add('/userlist', ['controller' => 'Home', 'action' => 'index']);
$router->add('/adduser', ['controller' => 'Home', 'action' => 'newUser']);
$router->add('/signup/create', ['controller' => 'Home', 'action' => 'create']);
$router->add('/user/{id:[\da-f]+}/remove', ['controller' => 'Home', 'action' => 'remove']);
$router->add('/user/{id:[\da-f]+}/edit', ['controller' => 'Home', 'action' => 'edit']);
$router->add('/signup/update', ['controller' => 'Home', 'action' => 'update']);
//var_dump ($router->getRoutes ());

$router->add('{controller}/{action}');

$router->dispatch($_SERVER['REQUEST_URI']);
